from django.db import models
from django.contrib.auth.models import User


# Create your models here.
# capital for the first letter for the class/ model name is conventional
class Article(models.Model):
    """docstring for Article. Auto_now_add will automatically add the tiime and date"""
    title = models.CharField(max_length=100)
    slug = models.SlugField()
    body = models.TextField()
    date = models.DateTimeField(auto_now_add=True)
    author = models.ForeignKey(User, default=None, on_delete=False)
    # add in thumbnail later
    # add in author later
    # def __init__(self, arg):
    #     super(Article, self).__init__()
    #     self.arg = arg

    def __str__(self):
        return self.title

    def snippet(self):
        return self.body[:50] + '...'
